FROM php:7.4-fpm

# Arguments defined in docker-compose.yml
ARG user
ARG uid

# Install system dependencies
RUN apt-get update && apt-get install -y nano \
    git \
    curl \
    libpng-dev \
    libonig-dev \
    libxml2-dev \
    zip \
    unzip \
    libzip-dev \
    nginx \
    certbot \
    python3-certbot-nginx

# Install PHP extensions (including ext-intl and ext-zip)
RUN docker-php-ext-configure zip
RUN docker-php-ext-install pdo_mysql mbstring exif pcntl bcmath gd intl zip

# Clear cache
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# Get latest Composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

# Create system user to run Composer and Artisan Commands
RUN useradd -G www-data,root -u $uid -d /home/$user $user
RUN mkdir -p /home/$user/.composer && \
    chown -R $user:$user /home/$user

# Set working directory
WORKDIR /var/www

COPY . .
COPY .env.example .env

RUN composer install
# Genera la clave de la aplicación
RUN php artisan key:generate

# Borra la configuración en caché y la caché
RUN php artisan config:clear
RUN php artisan cache:clear

# Create a crontab file for running Laravel Scheduler
COPY crontab /etc/cron.d/laravel-scheduler

# Give the correct permissions to the crontab file
RUN chmod 0644 /etc/cron.d/laravel-scheduler

USER $user
